﻿using System;
using System.ComponentModel;
using System.Text;
using Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UDPGameTest
{
    [TestClass]
    public class ExtensionsTest
    {
        [TestMethod]
        public void _01_Append_test()
        {
            var s1 = "lalka";
            var s2 = "zatralen";
            var a = Encoding.UTF8.GetBytes(s1);
            var b = Encoding.UTF8.GetBytes(s2);
            ByteExtensions.Append(ref a,b);
            var res = Encoding.UTF8.GetBytes(s1 + s2);
            Assert.AreEqual(a, res, $"{a} != {res}");
        }
    }
}
