﻿using System;
using System.Diagnostics;
using System.Text;
using Extensions;

namespace CommonLibrary
{
    
    public static class Packets
    {
        public static IPacket Resolve(byte[] packetBytes)
        {
            var pInt = BitConverter.ToInt32(packetBytes, 0);
            var packetType = (EPacketType)pInt;
            IPacket _packet = null;
            switch (packetType)
            {
                case EPacketType.PlayerMoved:
                    _packet = new PlayerActionPackets.PlayerMovedPacket(packetBytes);
                    //var direction = (EDirection)BitConverter.ToInt32(packetBytes, 8);
                    //var id = BitConverter.ToInt32(packetBytes, 12);
                    //var pl = Players.First((p) => p.ID == id);
                    //pl.X = BitConverter.ToInt32(packetBytes, 16);
                    //pl.Y = BitConverter.ToInt32(packetBytes, 20);
                    //switch (direction)
                    //{
                    //    case EDirection.Down:
                    //        pl.MoveDown(1);
                    //        break;
                    //    case EDirection.Up:
                    //        pl.MoveUp(1);
                    //        break;
                    //    case EDirection.Right:
                    //        pl.MoveRight(1);
                    //        break;
                    //    case EDirection.Left:
                    //        pl.MoveLeft(1);
                    //        break;
                    //}
                    break;
                case EPacketType.Login:
                    _packet = new ServerPackets.LoginPacket(packetBytes);
                    //var plaer = new Player(
                    //    BitConverter.ToInt32(packetBytes, 4),
                    //    BitConverter.ToChar(packetBytes, 8),
                    //    BitConverter.ToInt32(packetBytes, 10),
                    //    BitConverter.ToInt32(packetBytes, 14));
                    //Players.Add(plaer);
                    //ShitQueue.Add(plaer);
                    break;
                //case EPacketType.LoginResult:
                //    //Players.Add(new Player(
                //    //    BitConverter.ToInt32(packetBytes, 4),
                //    //    BitConverter.ToChar(packetBytes, 8),
                //    //    BitConverter.ToInt32(packetBytes, 10),
                //    //    BitConverter.ToInt32(packetBytes, 14)));
                    
                //    break;
                case EPacketType.PlayerList:
                    _packet = new ServerPackets.PlayerListPacket(packetBytes);
                //Players.Clear();
                //for (int i = 0; i < BitConverter.ToInt32(packetBytes, 4); i++)
                //{
                //    var pleer = new Player(BitConverter.ToInt32(packetBytes, 5 + i * 14),
                //        BitConverter.ToChar(packetBytes, 9 + i * 14),
                //        BitConverter.ToInt32(packetBytes, 11 + i * 14),
                //        BitConverter.ToInt32(packetBytes, 15 + i * 14));
                //    if (pleer.ID == Player.ID)
                //        Player = pleer;
                //    Players.Add(pleer);
                //}
                    break;
                default:
                    throw new UnknownPacketException(packetBytes);
            }
            var str = Encoding.UTF8.GetString(packetBytes);
            Debug.WriteLine(str);

            return _packet;
        }

        public class PlayerActionPackets
        {
            public class PlayerMovedPacket : IPacket
            {
                public EPacketType PacketType { get { return EPacketType.PlayerMoved; } }
                public int Length { get { return 14; }}
                public EDirection Direction { get; private set; }
                public int PlayerID { get; private set; }
                public int XCoord { get; private set; }
                public int YCoord { get; private set; }

                private byte[] _bytes;
                public byte[] Bytes
                {
                    get { return _bytes; }
                    set { Construct(value); } 
                }

                public void Construct(byte[] packetBytes)
                {
                    _bytes = packetBytes;
                    PlayerID = BitConverter.ToInt32(packetBytes, 1);
                    Direction = (EDirection)packetBytes[5];
                    XCoord = BitConverter.ToInt32(packetBytes, 6);
                    YCoord = BitConverter.ToInt32(packetBytes, 10);
                }

                public PlayerMovedPacket(byte[] packetBytes)
                {
                    Construct(packetBytes);
                }

                public PlayerMovedPacket(EDirection dir, int id, int x, int y)
                {
                    Direction = dir;
                    PlayerID = id;
                    XCoord = x;
                    YCoord = y;
                    _bytes = new[] { (byte)PacketType };
                    ByteExtensions.Append(ref _bytes,
                        BitConverter.GetBytes(PlayerID),
                        new[] { (byte)Direction },
                        BitConverter.GetBytes(XCoord),
                        BitConverter.GetBytes(YCoord));
                }
            }
                
            public class ShootPacket : IPacket
            {
                private byte[] _bytes;
                public EPacketType PacketType { get; }
                public int Length { get; }
                public byte[] Bytes { get { return _bytes; } set {Construct(value);} }

                public void Construct(byte[] packetBytes)
                {
                    _bytes = packetBytes;
                    throw new NotImplementedException();
                }

            }
        }
        public class ServerPackets
        {
            public class LoginPacket : IPacket
            {
                public EPacketType PacketType { get { return EPacketType.Login; } }
                public int Length { get { return 15; } }
                private byte[] _bytes;
                public byte[] Bytes
                {
                    get { return _bytes; }
                    set { Construct(value); }
                }
                public int PlayerID { get; private set; }
                public char PlayerSkin { get; private set; }
                public int XCoord { get; private set; }
                public int YCoord { get; private set; }

                public void Construct(byte[] packetBytes)
                {
                    _bytes = packetBytes;
                    PlayerID = BitConverter.ToInt32(_bytes, 1);
                    PlayerSkin = BitConverter.ToChar(_bytes, 5);
                    XCoord = BitConverter.ToInt32(_bytes, 7);
                    YCoord = BitConverter.ToInt32(_bytes, 11);
                }

                public LoginPacket(byte[] packetBytes)
                {
                    Construct(packetBytes);
                }

                public LoginPacket(int playerID, char playerSkin, int x, int y)
                {
                    PlayerID = playerID;
                    PlayerSkin = playerSkin;
                    XCoord = x;
                    YCoord = y;
                    _bytes = new[] { (byte)PacketType };
                    ByteExtensions.Append(ref _bytes,
                        BitConverter.GetBytes(PlayerID),
                        BitConverter.GetBytes(PlayerSkin),
                        BitConverter.GetBytes(XCoord),
                        BitConverter.GetBytes(YCoord));
                }
            }

            public class PlayerListPacket : IPacket
            {
                public EPacketType PacketType { get; }
                public int Length { get; private set; }
                private byte[] _bytes;
                public byte[] Bytes
                {
                    get{ return _bytes; }
                    set{ Construct(value); }
                }
                public int PlayersCount { get; private set; }
                public int[] PlayerIDs { get; private set; }
                public char[] PlayerSkins { get; private set; }
                public int[] XCoords { get; private set; }
                public int[] YCoords { get; private set; }

                public void Construct(byte[] packetBytes)
                {
                    _bytes = packetBytes;
                        
                }

                public PlayerListPacket(byte[] packetBytes)
                {
                    
                }

                public PlayerListPacket(int playersCount, int[] playerIDs, char[] playerSkins, int[] xCoords,
                    int[] yCoords)
                {
                    PlayersCount = playersCount;
                    PlayerIDs = playerIDs;
                    PlayerSkins = playerSkins;
                    XCoords = xCoords;
                    YCoords = yCoords;
                    Length = 1 + 4 + PlayersCount*14;
                }


            }
        }

        public static Player Player(this Packets.ServerPackets.LoginPacket packet)
        {
            return new Player(packet.PlayerID, packet.PlayerSkin, packet.XCoord, packet.YCoord);
        }
    }

    public class UnknownPacketException : Exception
    {
        public byte[] Packet { get; private set; }
        public UnknownPacketException(byte[] packet)
        {
            Packet = packet;
        }
    }

    public interface IPacket
    {
        EPacketType PacketType { get; }
        int Length { get; }
        byte[] Bytes
        {
            get; set; }

        void Construct(byte[] packetBytes);
    }
}