﻿using System;

namespace CommonLibrary
{
    public class Player
    {
        public int ID;
        public int X;
        public int Y;
        public char Skin;

        public EventHandler PlayerMoved = null;

        private void OnPlayerMovedEvent()
        {
            PlayerMoved?.Invoke(this, new EventArgs());
        }

        public Player(int id, char skin, int x, int y)
        {
            ID = id;
            Skin = skin;
        }

        public void MoveDown(int count)
        {
            Y += count;
        }

        public void MoveUp(int count)
        {
            Y -= count;
        }

        public void MoveRight(int count)
        {
            X += count;
        }

        public void MoveLeft(int count)
        {
            X -= count;
        }
    }
}