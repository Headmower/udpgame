﻿namespace CommonLibrary
{
    public enum EDirection : byte
    {
        Up,
        Down,
        Right,
        Left
    }
}