﻿namespace CommonLibrary
{
    public enum EPacketType : byte
    {
        Login = 0,
        PlayerList = 1,
        PlayerMoved = 2
    }
}