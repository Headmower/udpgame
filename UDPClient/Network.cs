﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using CommonLibrary;

namespace UDPClient
{
    public class Network
    {
        public UdpClient Client { get; private set; }
        public BlockingCollection<IPacket> SyncQueue = new BlockingCollection<IPacket>();
        public BlockingCollection<string> OutputPacketQueue = new BlockingCollection<string>();
        public Task Listener { get; private set; }
        public IPAddress IPAddress { get; private set; }
        public int Port { get; private set; }

        public EventHandler<Packets.ServerPackets.LoginPacket> PlayerLogin;
        public EventHandler<Packets.ServerPackets.PlayerListPacket> PlayerList;
        public EventHandler<Packets.PlayerActionPackets.PlayerMovedPacket> PlayerMoved;

        public Network(IPAddress ipAddress, int port)
        {
            IPAddress = ipAddress;
            Port = port;
            Client = new UdpClient(new IPEndPoint(IPAddress.Parse("127.0.0.1"), Port));
            Client.Connect(IPAddress, Port);
        }

        public void Start()
        {
            Listener = new Task(ListenerProc);
            Listener.Start();
        }

        public void SendPacket(IPacket packet)
        {
            switch (packet.PacketType)
            {
                case EPacketType.Login:

                    break;
                case EPacketType.PlayerList:

                    break;
                case EPacketType.PlayerMoved:

                    break;
            }
            Client.Send(packet.Bytes, packet.Length);
        }

        private void ListenerProc()
        {
            var serverEP = new IPEndPoint(IPAddress, 0);
            var buffer = new byte[1024];
            while (true)
            {
                var packet = Packets.Resolve(Client.Receive(ref serverEP));
                switch (packet.PacketType)
                {
                    case EPacketType.Login:
                        PlayerLogin?.Invoke(this, (Packets.ServerPackets.LoginPacket) packet);
                        //var loginPacket = (Packets.ServerPackets.LoginPacket) packet;
                        //ObjectQueue.Add(new Player(loginPacket.PlayerID, loginPacket.PlayerSkin, loginPacket.XCoord, loginPacket.YCoord));
                        break;
                    case EPacketType.PlayerList:
                        PlayerList?.Invoke(this, (Packets.ServerPackets.PlayerListPacket) packet);
                        break;
                    case EPacketType.PlayerMoved:
                        PlayerMoved?.Invoke(this, (Packets.PlayerActionPackets.PlayerMovedPacket) packet);
                        break;
                }

            }
        }
    }
}