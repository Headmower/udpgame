﻿using System;

namespace Extensions
{
    public static class ByteExtensions
    {

        public static void Append(ref byte[] source, params byte[][] addition)
        {
            foreach (var array in addition)
            {
                var len = source.Length;
                Array.Resize(ref source, source.Length + array.Length);
                Buffer.BlockCopy(array,0,source,len,array.Length);
                
            }
        }

        public static unsafe void Append2(byte* source, params byte*[] addition)
        {
            var length = 0;
            foreach (var array in addition)
            {
            }
        }
    }
}