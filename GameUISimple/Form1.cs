﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameUISimple
{
    public partial class Form1 : Form
    {
        public Game Game;
        public Form1()
        {
            InitializeComponent();
            //textBox1.Stre
            Game = new Game(IPAddress.Parse("31.14.22.23"), 13666);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Game.StartGame();
        }
    }
}
