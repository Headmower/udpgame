﻿using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Net;
using System.Threading;
using System.Timers;
using CommonLibrary;
using UDPClient;
using Timer = System.Timers.Timer;

namespace GameUISimple
{
    public class Game
    {
        public IPAddress IPAddress { get; private set; }
        public int Port { get; private set; }
        public BlockingCollection<string> OutputPacketQueue = new BlockingCollection<string>(128);
        public BlockingCollection<object> SyncQueue = new BlockingCollection<object>(128);
        public Network Network;
        public Player Player = null;
        public ObservableCollection<Player> Players = new ObservableCollection<Player>();
        private bool _logged = false;

        public Timer DrawTimer = new Timer(1f / 60) { AutoReset = true };

        public Game(IPAddress ip, int port)
        {
            IPAddress = ip;
            Port = port;
            Network = new Network(IPAddress, Port);
        }

        public void StartGame()
        {
            Network.PlayerLogin += PlayerLogin;
            Network.PlayerList += PlayerListReceived;

            Network.Start();
            Login();

            DrawTimer.Elapsed += DrawTimerOnElapsed;
            DrawTimer.Start();
            GameLoop();
            
        }

        private void PlayerLogin(object sender, Packets.ServerPackets.LoginPacket loginPacket)
        {
            Player = loginPacket.Player();
            _logged = true;
        }

        private void Login()
        {
            Console.WriteLine("Выберите скин игрока:");
            var skin = Console.ReadKey(false).KeyChar;
            Console.WriteLine("\r\nВы выбрали скин. Ожидайте подключения к серверу.");
            Network.SendPacket(new Packets.ServerPackets.LoginPacket(0, skin, 0, 0));
            while (!_logged)
            {
                Console.Write(".");
                Thread.Sleep(200);
            }
        }

        private void PlayerListReceived(object sender, Packets.ServerPackets.PlayerListPacket playerListPacket)
        {

        }

        private void DrawTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Console.Clear();
            foreach (var player in Players)
            {
                Console.SetCursorPosition(player.X, player.Y);
                Console.Write(player.Skin);
            }
        }

        private void GameLoop()
        {
            while (true)
            {
                var k = Console.ReadKey(true);
                var packetData = new byte[1024];
                switch (k.Key)
                {
                    case ConsoleKey.DownArrow:
                        //Player.MoveDown(1);
                        Network.SendPacket(new Packets.PlayerActionPackets.PlayerMovedPacket(
                            EDirection.Down, Player.ID, Player.X, Player.Y));
                        break;
                    case ConsoleKey.UpArrow:
                        //Player.MoveUp(1);
                        Network.SendPacket(new Packets.PlayerActionPackets.PlayerMovedPacket(
                            EDirection.Up, Player.ID, Player.X, Player.Y));
                        break;
                    case ConsoleKey.RightArrow:
                        //Player.MoveRight(1);
                        Network.SendPacket(new Packets.PlayerActionPackets.PlayerMovedPacket(
                            EDirection.Right, Player.ID, Player.X, Player.Y));
                        break;
                    case ConsoleKey.LeftArrow:
                        //Player.MoveLeft(1);
                        Network.SendPacket(new Packets.PlayerActionPackets.PlayerMovedPacket(
                            EDirection.Left, Player.ID, Player.X, Player.Y));
                        break;
                }
                //OutputPacketQueue.Add($"Была нажата клавиша {k.Key}");
                //var packet = OutputPacketQueue.Take();
            }
        }
    }
}